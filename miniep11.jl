using Test

function teste()
	@test palindromo("") == true
	@test palindromo("ovo") == true
	@test palindromo("MiniEP11") == false
	@test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
	@test palindromo("A mãe te ama.") == true
	@test palindromo("Passei em MAC0110!") == false
	println("fim")
end

function tira(s,i)
	a = s[1:i-1]
	b = s[i+1:end]
	return a*b
end

using Unicode

function palindromo(string)
	a = lowercase(string)
	s = Unicode.normalize(a,stripmark = true)
	i = length(s)
	while i > 0
		if s[i] == ' '||s[i] == '.'||s[i] == '!'||s[i] == '?'|| s[i] == ','|| s[i] == ':'||s[i] == ';'||s[i] == '-'
			s = tira(s,i)
		end
		i = i - 1
	end
	for j in 1 : div(length(s),2)
		if s[j] != s[length(s) - j + 1]
			return false
		end
	end
	return true
end
